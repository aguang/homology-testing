all	// simulate all, ST->GT, GT->GS, ST->GS or GS->RR
	// input directory (leave blank if none)
y	// save intermediate products? (y/n)
5	// number of gene families
===Species Tree Parameters===
./test/trees // directory for species tree and gene trees
===Gene Sequence Parameters===
./test/genesequences	// directory for out files
===Raw Reads Parameters===
./	// directory with RNAseqreadsimulator scripts
test/reference		// directory for out files
100		// read length
5000		// number of reads to simulate
0		// read error
n		// use explv script? (y/n)
